# Basic Entity
##### À voir si attribut non nécessaire (tout les classes extend B.E.)
+ CreatedAt & By
+ UpdatedAt & By
+ DeletedAt & By

# Cards 
- Front-Content : `CardContent`
	+ `Content-Type` : string -- enum (permettre les images ?)
	+ `Content-Value` : string
- Back-Content : `CardContent`
	+ `Content-Type` : string -- enum (permettre les images ?)
	+ `Content-Value` : string
+ DeckId
+ `LastSuccessTime`
+ `LastChallengeTime`
+ `NextChallengeTime`

# Deck
+ Name : string
+ Cards : Card[]
+ Color : string (HEX value) 
+ UserId 

# User
- UserId
- Decks : Deck[]
- `Auth attributes`

# Furasshukado Roadmap 

## Phase 1 : Desktop Prototype
----
## Shortcuts are important for Worflow
- Workflow (only desktop) `(finger variant for mobile ?)`
`Ctrl-N` : Create a new flash card.
`Ctrl-Shift-N` : Create a new deck
`Ctrl-D/delete` : Delete card

- Flash card
`Ctrl-R/Tap` : Rename Field
`Double Tap` : Turn card around
`Swipe right/->` : Next Card
`Swipe left/<-` : Prev card


## Phase 2
----
## Desktop and mobile

- Cards and Decks can be synced and shared accross desktop and mobile devices.
- Algorithm for chunking workloads (pex : 100 pages à étudier d'ici 15 avril)


## OCR 
Take a picture of a text book to create a flashcard.

## Translate with Google
Translate your cards in a language you wish to learn.

## Phase 3
---
## Social
Connect with other learners just like YOU. Share decks and learn together.

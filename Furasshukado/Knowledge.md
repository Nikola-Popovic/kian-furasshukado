# Space Repetition

The _spacing effect_ was reported by a German psychologist Hermann Ebbinghaus in 1885. 
He observed that we tend to remember things more effectively, if we spread reviews out over time, instead of studying multiple times in one session. _His groundbreaking work on the forgetting curve was published in his paper “[Memory: A Contribution to Experimental Psychology](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4117135/)” in 1885._ 

_"It’s not necessary, or practical, to try to memorize everything within a single session."_ 

Since the 1930s, there have been a number of proposals for utilizing the spacing effect to improve learning, in what has come to be called _spaced repetition_.

## Benefits
-  Continually re-exposes you to information at spaced intervals.
-   Helps build longer lasting memories
-   Increases the time actively rehearsing a memoryrather than passively consuming information.
-   Having a predictable spaced repetition schedule teaches your brain to predict when it will next see the material and respond with greater alertness and attention, making it easier for information in that time frame to be encoded into long-term memory.
-   The technique allows you to break up larger tasks into smaller chunks of work spaced at intervals throughout the day (e.g., break one chapter into three sections): an approach called [chunking](https://link.springer.com/referenceworkentry/10.1007%2F978-1-4419-1428-6_1042).

## Half-Life Regression

Half-life regression (HLR) is a new spaced repetition algorithm developed by researchers from the [Duolingo](https://www.duolingo.com/) language learning website and app. Backed by data from the [40+ million](https://blog.duolingo.com/global-language-report-2020/) monthly active users of Duolingo, the half-life regression model was created to boost retention, increase student recall rates, and enhance learner engagement. Duolingo is great for picking up a second or third language within a convenient software package.

The research that Duolingo did to develop this model became a [published paper](https://research.duolingo.com/papers/settles.acl16.pdf) and further explanations on its effectiveness can be read there.


## Super Memo Algorithm

However, one of the most commons ways of choosing spacing intervals involves doubling the spacing after every interval. At its most basic,  it’s the algorithm that SuperMemo uses and it involves these repetitions:

-   First repetition: 1 day
-   Second repetition: 7 days
-   Third repetition: 16 days
-   Fourth repetition: 35 days

## Planning study gaps
However, due to its lack of customization and the inability to create your own flashcards, it’s not exactly a competitor to open-source spaced repetition software such as Anki. The research that Duolingo did to develop this model became a [published paper](https://research.duolingo.com/papers/settles.acl16.pdf) and further explanations on its effectiveness can be read there. For the purposes of language learning, though, Duolingo’s algorithms are certainly worth checking out.

![[Pasted image 20220130165041.png]]

So if you’ve got a test coming up in a week, you should do your first session today, and then do the next session either tomorrow or the day after. I’d also recommend adding a 3rd session the day before the test.

It’s important to know that these gaps are _approximate –_ as with pretty much everything in brain/memory science, it’s hard to make completely specific recommendations that will work for everyone. Still, these numbers are close, and you should take them into account when you’re creating [exam study schedules](https://collegeinfogeek.com/final-exam-study-schedule/).

## Sources
[Tamm, Sender. "What is the Ebbinghaus Forgetting Curve ?" ](https://e-student.org/ebbinghaus-forgetting-curve/)
[Spaced Repeition](https://e-student.org/spaced-repetition/)
[Anki Docs](https://docs.ankiweb.net/background.html#spaced-repetition): 
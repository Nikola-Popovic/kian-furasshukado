

<div align="center">
<h4> furasshukādo </h4>
<strong> フラッシュカード - </strong>
<br/>
<i> Learning by spaced repetition </i>
<a  href="https://gitlab.com/Nikola-Popovic/kian-furasshukado/-/tree/main/Furasshukado"><strong> Explore the docs »</strong></a>
</div>

---

## About the project
The project _furasshukādo_ aims to facilate learning by spaced repetition through the creation of flash cards. 
<p align="right">(<a href="#top">back to top</a>)</p>
  
### Built With

* [React.js](https://reactjs.org/)

* [elixir](https://elixir-lang.org/)


## Getting Started

To get a local copy up and running follow these simple example steps.

### Prerequisites

* npm

```sh

npm install npm@latest -g

```

### Installation

1. Clone the repo

```sh

git clone https://github.com/github_username/repo_name.git

```

2. Install NPM packages

```sh

npm install

```


<p align="right">(<a href="#top">back to top</a>)</p>

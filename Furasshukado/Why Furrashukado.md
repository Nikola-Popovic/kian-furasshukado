# Why should people bother ?

Other applications on the market have limitations when it comes to creating flash-cards rapidly on the go. 

We aim to _automate_ the most for creating flashcards.

	- Productivity shortcuts
	- Creating flashcards using camera and OCR
	- Easy syntax highlighting with MD support
	- Integration with Google Translate for Translations and pronunciations
	- Chunking workloads for spaced learning before an event (f.e. an exam)

